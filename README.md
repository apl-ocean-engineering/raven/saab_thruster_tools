# saab_thruster_tools

Scripts for interacting with the Saab thrusters on the Raven vehicle.  Note in general we **don't** need a complete solution for interacting with the thrusters -- this is provided by Greensea.  These are simple utilities for testing individual thrusters.

### Prerequisites

Scripts require [pyserial](https://pyserial.readthedocs.io/en/latest/index.html)


----
# Programs

## run_tilt.py

`run_tilt.py` allows interactive control of the tilt sensor.  It must be run on a computer with serial communication to the thruster (e.g. on the Opensea Hub on the Raven ROV).   Once running, it will show the serial commands **to** and **from** the thruster.

Once running, the program accepts the following inputs.

```
     <, >    : Increment position +/-10 degress
     0, z    : Set position to 0
     1-9     : Set position to +10-90 degreees
     !-(     : (number keys, but with "shift"ed)  Set position to -10 to -90 degrees

     q       : Quit program.   Does not move thruster when quitting
```



----
----

# Thruster Protocol

All thrusters are on a multi-drop half-duplex RS485 network.  They are at 57600-8-N-1 (the factory default).

Commands **to** the thrusters are as follows:

```
uXNcccc\r
```

With:

|   |                |
| - | -------------- |
| `u` | The letter 'u' |
| `X` | The thruster ID, in the range of '0'-'9' and 'A'-'Z'.  '0' is a broadcast address. |
| `N` | The **total** number of bytes in the message, including the carriage return, as a single hex digit.  Note the shortest possible message is 4 == `uX4\r` |
| `cccc` | A variable number of command characters |
| `\r` | Carriage return | 

The response from the thruster is in the same format but will start with `U`.   The ID will be the ID of the *responding* thruster.  In the table below, *(ack)* means a minimal message: `UX4\r`

The thrusters recognize the following commands (from the manual):

| Command | Reply | Description |
| ------- | ----- | ----------- |
| `+nnn` | (ack) | Forward demand 0-100% |
| `-nnn` | (ack) | Reverse demand 0-100% |
| `?R` | 0 to 6250 | Motor RPM (divide by 5 to give prop RPMs) |
| `?C` | 0 to 10 | 0 to 6 Amps |
| `?b` | | Baud rate |
| `?h` | `hH mM` | Total run time.  H = hours, M = minutes |
| `?n` | string | Node name (up to 8 chars) |
| `?v` | `mmnn` | Version number `mm` == major number, `nn` = minor number |
| `x0` | | Disable eeprom write |
| `x1` | | Enable eeprom write -- req'd to change baud or id |
| `z` | Non-zero on success | Reset node |
| `nX` | Non-zero on success | Set node address.  Eeprom write must be enabled.  Takes effect after a reset. | 

*(plus some commands for changing the baud rate which we don't use)*


# Tilt Motor protocol

We do not currently have a manual for the tilt motor.   This is what we've figured out so far:

The tilt motor protocol mirrors the thruster protocol but uses `t` rather than `u`

Commands **to** the tilt motor are as follows:

```
tXNcccc\r
```

With:

|   |                |
| - | -------------- |
| `t` | The letter 't' |
| `XNcccc\r` | As above for thruster |

Thus far we've found:

| Command | Reply | Description |
| ------- | ----- | ----------- |
| `?`  | `n` | Unsure of function.  Our tilt motor returns '0' |
| `?v` | `mmnn` | Version number `mm` == major number, `nn` = minor number |
| `+nnn` | ?? | Position (in degrees)? |
| `-nnn` | ?? | Position (in degrees)? |

The reply **from** the motor for a position command will be:

```
TXNccc\r
```

With

|   |                |
| - | -------------- |
| `T` | The letter 'T' |
| `X` | Tilt motor ID |
| `N` | Total length of the packet (including the \r) |
| `ccc` | **Current** position of the tilt motor, in integer degrees |

For example:

```
T159\r
```

Means tilt motor 1 is at +9 degrees

```
T17-12\r
```

Means tilt motor 1 is at -12 degrees
