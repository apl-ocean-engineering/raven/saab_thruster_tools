#!/usr/bin/env python3
#
# Given two valid thruster IDs, X and Y on the command line, will reprogram
# thruster X to have ID Y.

import serial
import argparse
import re
import time



if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Change a Saab thruster\'s CAN bus ID')
    parser.add_argument('oldid', 
                        help='ID of thruster')

    parser.add_argument('newid', 
                        help='New ID of thruster')

    args = parser.parse_args()
    
    oldid = args.oldid.upper()[0]
    newid = args.newid.upper()[0]

    pattern = re.compile("[A-Z0-9]")
    if pattern.fullmatch(oldid) is None:
        print("Invalid old id \"%c\"" % oldid)
        exit()
    
    if pattern.fullmatch(newid) is None:
        print("Invalid new id \"%c\"" % newid)
        exit()
    
    print("Changing thruster %c to %c" % (oldid,newid))

    oldid = bytes(oldid, "utf-8")
    newid = bytes(newid, "utf-8")   

    with serial.Serial('/dev/ttyMAX1', 57600, timeout=1) as ser:

        query = b"u%c6x1\r" % (oldid)
        ser.write(query)
        print("--> ", query)

        response = ser.read(20)
        print("<--", response)

        query = b"u%c6n%c\r" % (oldid, newid)
        ser.write(query)
        print("--> ", query)

        response = ser.read(20)
        print("<--", response)


        ## Try to disable EEPROM write on old id
        query = b"u%c6x0\r" % (oldid)
        ser.write(query)
        print("--> ", query)

        response = ser.read(20)
        print("<--", response)

        ## Try to disable EEPROM write on new id
        query = b"u%c5z\r" % (oldid)
        ser.write(query)
        print("--> ", query)

        response = ser.read(20)
        print("<--", response)



