#!/usr/bin/env python3
#
# Sequentially requests the version of thruster ID 1-9, A-Z and prints reponse.
# If it receives a response, it also requests the thruster name.
#
# Runs through all ID, does not stop afer first response.

import serial

with serial.Serial('/dev/ttyMAX1', 57600, timeout=1) as ser:

    for i in range(0,9):
        query = b"u%1d6?v\r" % i
        ser.write(query)
        print("--> ", query)

        response = ser.read(20)
        print("<--", response)

        if len(response) > 0:
            query = b"u%1d6?n\r" % i
            ser.write(query)
            print("--> ", query)

            response = ser.read(20)
            print("<--", response)

    for i in range(ord('A'), ord('Z')+1):
        id = bytes(chr(i), 'utf-8')

        query = b"u%c6?v\r" % id
        ser.write(query)
        print("--> ", query)

        response = ser.read(20)
        print("<--", response)

        if len(response) > 0:
            query = b"u%c6?n\r" % id
            ser.write(query)
            print("--> ", query)

            response = ser.read(20)
            print("<--", response)
