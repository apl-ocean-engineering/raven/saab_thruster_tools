#!/usr/bin/env python3
#
# Simple tilt motor test function.
#  Given a tilt motor ID (1-9, A-Z) it moves the motor to  +(degrees) for 
#  (duration) seconds;  then turns to at -(degrees) for (duration) seconds,
#  then moves to zero.
# 
# The Raven tilt motor is ID 1
#
# Sample commands:
#
#   t18+000\r     => Send to 0 degrees
#
#
#   Once running, the program will take keyboard inputs:
#     <, >    : Increment position +/-10 degress
#     0, z    : Set position to 0
#     1-9     : Set position to +10-90 degreees
#     !-(     : (number keys, but with "shift"ed)  Set position to -10 to -90 degrees
#
#     q       : Quit program.   Does not move thruster when quitting
#
# !!NOTE!!  This will cause the motor to spin immediately.  Please ensure the
# thruster blades are clear before running script!

import serial
import argparse
import re
import time

import curses

demand = 0


def main(stdscr):
    parser = argparse.ArgumentParser(description='Briefly run a Saab tilt motor')
    parser.add_argument('--id', default='1', nargs="?",
                        help='ID of tilt motor')
    parser.add_argument('--rate', 
                        default=0, type=float,
                        help='Duration of cycle (seconds)')
    parser.add_argument('--degrees', '-p', 
                        default=0, type=int,
                        help='Initial position in degrees')

    args = parser.parse_args()
    
    demand = args.degrees

    id = args.id.upper()[0]

    pattern = re.compile("[A-Z0-9]")
    if pattern.fullmatch(id) is None:
        print("Invalid id \"%c\"" % id)
        exit()
    
    print("Blipping tilt motor %c" % id)
    id = bytes(id, "utf-8")

    stdscr.nodelay(1)
    step = 10

    with serial.Serial('/dev/ttyMAX1', 57600, timeout=1) as ser:

        done = False;
        while not done:

            if demand >= 0:
                query = b"t%c8+%03d\r" % (id, demand)
            else:
                query = b"t%c8-%03d\r" % (id, abs(demand))
            ser.write(query)
            print("--> %s\r" % query.rstrip())

            response = ser.read(20)
            print("<--%s\r" % response.rstrip())

            c = stdscr.getch()
            if c != -1:
                c=chr(c)
                print(c)
                if c=='<':
                    demand = demand-step
                elif c=='>':
                    demand = demand+step
 
                elif c=='z' or c=='0':
                    demand = 0
 

                elif c=='1':
                    demand = 10
                elif c=='2':
                    demand = 20
                elif c=='3':
                    demand = 30
                elif c=='4':
                    demand = 40
                elif c=='5':
                    demand = 50
                elif c=='6':
                    demand = 60
                elif c=='7':
                    demand = 70
                elif c=='8':
                    demand = 80
                elif c=='9':
                    demand = 90

                elif c=='!':
                    demand = -10
                elif c=='@':
                    demand = -20
                elif c=='#':
                    demand = -30
                elif c=='$':
                    demand = -40
                elif c=='%':
                    demand = -50
                elif c=='^':
                    demand = -60
                elif c=='&':
                    demand = -70
                elif c=='*':
                    demand = -80
                elif c=='(':
                    demand = -90


                elif c=='q':
                    done = True
                
                continue

            time.sleep(args.rate)


if __name__ == '__main__':
    curses.wrapper(main)
