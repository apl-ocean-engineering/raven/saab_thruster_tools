#!/usr/bin/env python3
#
# Simple tilt motor test function.
#  Given a tilt motor ID (1-9, A-Z) it commands the motor to (degrees), prints the 
#  response, and exits.
#
# !!NOTE!!  This will cause the motor to spin immediately.  Please ensure the
# thruster blades are clear before running script!

import serial
import argparse
import re
import time



if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Briefly run a Saab tilt motor')
    parser.add_argument('id', default='1',
                        help='ID of tilt motor')
    parser.add_argument('--duration', '-d', 
                        default=2, type=int,
                        help='Duration to hold at each position (seconds)')
    parser.add_argument('degrees', 
                        default=0, type=int,
                        help='Degrees position')

    args = parser.parse_args()
    
    id = args.id.upper()[0]

    pattern = re.compile("[A-Z0-9]")
    if pattern.fullmatch(id) is None:
        print("Invalid id \"%c\"" % id)
        exit()
    
    print("Setting tilt motor %c to %d" % (id,args.degrees))
    id = bytes(id, "utf-8")

    with serial.Serial('/dev/ttyMAX1', 57600, timeout=1) as ser:

        demand = args.degrees
        reps = args.duration

        if demand < 0:
            query = b"t%c8-%03d\r" % (id, -demand)
        else:
            query = b"t%c8+%03d\r" % (id, demand)

        ser.write(query)
        print("--> ", query)

        response = ser.read(20)
        print("<--", response)
