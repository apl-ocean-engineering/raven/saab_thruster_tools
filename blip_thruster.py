#!/usr/bin/env python3
#
# Simple thruster test function.
#  Given a thruster ID (1-9, A-Z) it runs the thruster at +(percent effort) for 
#  (duration) seconds;  then runs at -(percent effort) for (duration) seconds.
#
# !!NOTE!!  This will cause the thruster to spin immediately.  Please ensure the
# thruster blades are clear before running script!

import serial
import argparse
import re
import time



if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Briefly run a Saab thruster')
    parser.add_argument('id', 
                        help='ID of thruster')
    parser.add_argument('--duration', '-d', 
                        default=1, type=int,
                        help='Duration to run thruster in each direction (seconds)')
    parser.add_argument('--percent', '-p', 
                        default=20, type=int,
                        help='Percent effort')

    args = parser.parse_args()
    
    id = args.id.upper()[0]

    pattern = re.compile("[A-Z0-9]")
    if pattern.fullmatch(id) is None:
        print("Invalid id \"%c\"" % id)
        exit()
    
    print("Blipping thruster %c" % id)
    id = bytes(id, "utf-8")

    with serial.Serial('/dev/ttyMAX1', 57600, timeout=1) as ser:

        demand = args.percent
        reps = args.duration

        for i in range(reps):
            query = b"u%c8+%03d\r" % (id, demand)
            ser.write(query)
            print("--> ", query)

            response = ser.read(20)
            print("<--", response)

            time.sleep(1)

        for i in range(reps):
            query = b"u%c8-%03d\r" % (id, demand)
            ser.write(query)
            print("--> ", query)

            response = ser.read(20)
            print("<--", response)

            time.sleep(1)

        query = b"u%c8+%03d\r" % (id, 0)
        ser.write(query)
        print("--> ", query)

        response = ser.read(20)
        print("<--", response)
